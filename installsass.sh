#!/bin/sh
sudo apt-get update
sudo apt-get install build-essential zlib1g-dev git-core
curl -L https://get.rvm.io | bash -s stable --ruby
source /home/vagrant/.rvm/scripts/rvm
source ~/.profile
rvm use 2.0.0
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
grunt watch

