<?php

$EM_CONF[$_EXTKEY] = [
  'title' => 'Site Resources',
  'description' => 'Functionality and resources for the website.',
  'category' => 'misc',
  'state' => 'stable',
  'version' => '1.0.0',
   'constraints' => [
     'depends' => [
       'jgrp_base' => '0.0.0',
     ],
   ],
];
