lib.page {
  template {
    layoutRootPaths {
      20 = EXT:jgrp_site/Resources/Private/Layouts/Page
    }

    partialRootPaths {
      20 = EXT:jgrp_site/Resources/Private/Partials/Page
    }

    templateRootPaths {
      20 = EXT:jgrp_site/Resources/Private/Templates/Page
    }
  }
}
