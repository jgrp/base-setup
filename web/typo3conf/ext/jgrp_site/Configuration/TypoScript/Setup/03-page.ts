page {
  includeCSS {
    main = EXT:jgrp_site/Resources/Public/Css/main.css
  }

  footerData {

    # Not using includeJS here since we need "data-main"
    2 = FLUIDTEMPLATE
    2 {
      file = EXT:jgrp_site/Resources/Private/Templates/JavaScript.html
      extbase.controllerExtensionName = JgrpSite
  }

}
