requirejs.config({
  paths: {
    mdl: [
      "https://code.getmdl.io/1.3.0/material.min",
    ],
    jquery: [
      "https://code.jquery.com/jquery-3.2.1.min",
    ],
    "magnificpopup-lib": [
      "/typo3conf/ext/jgrp_site/Resources/Public/JavaScript/Lib/magnific-popup.min",
    ],
    lightbox: [
      "/typo3conf/ext/jgrp_base/Resources/Public/JavaScript/magnific-popup",
    ],
  },
  shim: {
    mdl: ["jquery"],
    "magnificpopup-lib": ["jquery"],
    lightbox: ["magnificpopup-lib"],
  },
  // Prevent RequireJS from Caching Required Scripts
  urlArgs: "bust=v2"
});


require(["mdl", "lightbox"]);
