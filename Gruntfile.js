module.exports = function(grunt) {
  //require('jit-grunt')(grunt);

  grunt.initConfig({
    paths: {
      public: "web/typo3conf/ext/jgrp_site/Resources/Public/",
      private: "web/typo3conf/ext/jgrp_site/Resources/Private/",
      base_public: "web/typo3conf/ext/jgrp_base/Resources/Public/",
      base_private: "web/typo3conf/ext/jgrp_base/Resources/Private/",
      sass: "<%= paths.private %>Sass/",
      fonts: "<%= paths.public %>fonts/",
    },
    uglify: {
      js: {
        files: [{
          expand: true,
          cwd: '<%= paths.private %>JavaScript/',
          src: '**/*.js',
          dest: '<%= paths.public %>JavaScript/',
        },
        {
          expand: true,
          cwd: '<%= paths.base_private %>JavaScript/',
          src: '**/*.js',
          dest: '<%= paths.base_public %>JavaScript/',
        }],
      }
    },

    watch: {
      sass: {
        files: ['<%= paths.private %>Sass/**/*.scss', '<%= paths.base_private %>Sass/**/*.scss'],
        tasks: 'sass',
      },
      js: {
        files: ['<%= paths.private %>JavaScript/**/*.js','<%= paths.base_private %>JavaScript/**/*.js'],
        tasks: 'uglify',
      },
      fonts: {
        files: ['<%= paths.private %>Fonts/**/*','<%= paths.base_private %>Fonts/**/*'],
        tasks: 'copy:fonts',
      }
    },

    clean: {
      css:   '<%= paths.public %>Css/**/*.css',
      fonts: '<%= paths.public %>Fonts/*',
      js:    '<%= paths.public %>JavaScript/**/*.js',
      photoswipe: '<%= paths.public %>Images/photoswipe/',
    },

    sass: {
      dist: {
        options: {
          style:'compressed' ,
          loadPath: 'node_modules'
        },
        files: [{
          expand: true,
          cwd: '<%= paths.private %>Sass/',
          src: ['*.scss'],
          dest: '<%= paths.public %>Css/',
          ext: '.css'
        },
        {
          expand: true,
          cwd: '<%= paths.base_private %>Sass/',
          src: ['*.scss'],
          dest: '<%= paths.base_public %>Css/',
          ext: '.css'
        }]
      }
    },

    npmcopy: {
      fonts : {
        options: {
          destPrefix: '<%= paths.public %>Fonts/',
        },
        files: {
          'material-design-icons': 'material-design-icons/iconfont/*',
        },
      },
      css: {
        files : {
          '<%= paths.public %>Css/': 'material-design-lite/material.min.css',
        },
      },
      javascript: {
        options: {
          destPrefix: '<%= paths.public %>JavaScript/Lib/',
        },
        files: {
          'material.min.js': 'material-design-lite/material.min.js',
          'jquery.min.js': 'jquery/dist/jquery.min.js',
          'magnific-popup.min.js': 'magnific-popup/dist/jquery.magnific-popup.min.js',
          'photoswipe.min.js' : 'photoswipe/dist/photoswipe.min.js',
          'photoswipe-ui-default.min.js' : 'photoswipe/dist/photoswipe-ui-default.min.js',
        },
      },
      images: {
        options: {
          destPrefix: '<%= paths.public %>Images/',
        },
        files: {
          'photoswipe/default-skin.svg': 'photoswipe/dist/default-skin/default-skin.svg',
          'photoswipe/default-skin.png': 'photoswipe/dist/default-skin/default-skin.png',
          'photoswipe/preloader.gif': 'photoswipe/dist/default-skin/preloader.gif',
        }
      }

    },

    copy: {
      fonts: {
        files:[{
          expand: true,
          cwd: '<%= paths.private %>Fonts/',
          src: ['**/*'],
          dest: '<%= paths.public %>Fonts/'
        }]
      }
    }
  });


  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-npmcopy');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask("build", [
    "clean", "npmcopy" , "sass", "uglify", "copy"
  ]);
  grunt.registerTask("default", [
    "build"
  ]);
};
