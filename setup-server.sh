# SRC
# https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04
# https://typo3.org/typo3-cms/overview/requirements/
# https://github.com/TYPO3/TYPO3.CMS/blob/TYPO3_8-7/INSTALL.md
# https://wiki.typo3.org/Performance_tuning


#
# create ssh user
# https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-14-04

adduser jgrp
gpasswd -a jgrp sudo
# add .ssh public-key
su - demoh
mkdir .ssh
chmod 700 .ssh
vim .ssh/authorized_keys
chmod 600 .ssh/authorized_keys
exit
service ssh restart

#
# install php packages
#

# install php 7.1
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt install php7.1 php7.1-common


# remove php 7.0
sudo apt-get purge php7.0 php7.0-common


# install apcu ?
#sudo apt-get install php-apcu

# install graphicsmagick
sudo apt-get install php-gmagick
sudo apt install graphicsmagick

# isntall GDLib
sudo apt-get install php-gd

# install ext-mbstring
apt install php-mbstring

# install ext-zip
apt install php-zip

# install ext-xml
sudo apt install php-xml

# install mysql
sudo apt install php7.1-mysql


# install Sass
sudo apt-get install ruby-full rubygems
sudo apt install ruby-sass

# install composer
sudo -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer
sudo -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer


# install nodejs and npm
sudo apt-get install nodejs
apt install npm
sudo ln -s /usr/bin/nodejs /usr/bin/node


# user rights
sudo chown -Rf jgrp:jgrp jgrp.de/
# typo3 relevante ordner für www-data freigeben, jgrp der gruppe www-date zuordnen

# activate mod_rewrite
sudo a2enmod rewrite

# Enable mod headers module
sudo a2enmod headers

# php uplpad file size
sudo vim /etc/php/7.1/apache2/php.ini
# upload_max_filesize = 50M

# install apache module mod_cloudflare
sudo apt-get install libtool apache2-dev
wget https://www.cloudflare.com/static/misc/mod_cloudflare/mod_cloudflare.c
sudo apxs -a -i -c mod_cloudflare.c

# restart apache 
sudo service apache2 restart